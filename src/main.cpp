#include <iostream>
#include <climits>
#include <fstream>
#include <cstring>
#include <ctime>
#include <iomanip>
#include <vector>
#include "../include/compression.h"

constexpr char USAGE_MESSAGE[] = "Usage: -c/-d INPUT-FILENAME [OUTPUT-FILENAME]";
constexpr char EXTENSION[] = "compressed";
constexpr std::size_t BUFFER_SIZE = 1U << 18U;

bool validate(int argc, char **argv) {
    if (argc < 3 or argc > 4) { return false; }
    if (std::strcmp(argv[1], "-c") != 0 and std::strcmp(argv[1], "-d") != 0) { return false; }
    if (std::strcmp(argv[1], "-d") == 0 and argc == 3) { return false; }
    return true;
}


int main(int argc, char **argv) {
    if (not validate(argc, argv)) {
        std::cout << USAGE_MESSAGE << std::endl;
        return 0;
    }

    enum {
        COMPRESS, DECOMPRESS
    } type;
    std::string source_name, destination_name;
    type = std::strcmp(argv[1], "-c") == 0 ? COMPRESS : DECOMPRESS;
    source_name = argv[2];
    destination_name = argc > 3 ? argv[3] : source_name + "." + EXTENSION;

    std::ifstream source;
    std::ofstream destination;
    source.open(source_name);
    destination.open(destination_name);

    std::clock_t time = clock();
    switch (type) {
        case COMPRESS:
            compress::encode(source, destination);
            break;
        case DECOMPRESS:
            try {
                compress::decode(source, destination);
            } catch (...) {
                std::cerr << "Couldn't decompress " + source_name << ": file is corrupt" << std::endl;
                return 0;
            }

            break;
        default:
            return -1; // There is a better choice, probably
    }

    std::cerr << "time taken: "
              << std::setprecision(3) << double(clock() - time) / CLOCKS_PER_SEC << "s."
              << std::endl;
}

