#include <gtest/gtest.h>
#include "../include/compression.h"

namespace {
    std::string encode_decode(std::string const &s) {
        std::istringstream in(s);
        std::ostringstream code_out;
        compress::encode(in, code_out);
        std::istringstream code_in(code_out.str());
        std::ostringstream out;
        compress::decode(code_in, out);
        return out.str();
    }
}
TEST(correctness, zero) {
    std::string s;
    EXPECT_EQ(s, encode_decode(s));
}

TEST(correctness, one) {
    std::string s{'\0'};
    EXPECT_EQ(s, encode_decode(s));
}

TEST(correctness, two) {
    std::string s(1, 22);
    EXPECT_EQ(s, encode_decode(s));
}

TEST(correctness, three) {
    std::string s{0, 1, 2};
    EXPECT_EQ(s, encode_decode(s));
}

TEST(correctness, letters) {
    std::string s("aazsd");
    EXPECT_EQ(s, encode_decode(s));
}

namespace {
    struct random_char {
        char operator()() { return char(std::rand()); }
    };

    std::string random_string(std::size_t size) {
        std::string s(size, 0);
        std::generate_n(s.begin(), size, random_char());
        return s;
    }
}

TEST(correctness, big_string) {
    std::string s = random_string(100000);
    EXPECT_EQ(s, encode_decode(s));
}

TEST(correctness, random_strings_test) {
    static constexpr std::size_t MAX_LENGTH = 100;
    static constexpr std::size_t REPETITIONS_COUNT = 10;
    for (std::size_t len = 1; len < MAX_LENGTH; ++len) {
        for (std::size_t _ = 0; _ < REPETITIONS_COUNT; ++_) {
            std::string s = random_string(len);
            EXPECT_EQ(s, encode_decode(s));
        }
    }
}

int main(int ac, char *av[]) {
    testing::InitGoogleTest(&ac, av);
    return RUN_ALL_TESTS();
}

