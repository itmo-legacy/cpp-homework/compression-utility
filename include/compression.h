#ifndef COMPRESSION_UTILTITY_COMPRESSION_H
#define COMPRESSION_UTILTITY_COMPRESSION_H

#include <istream>
#include <ostream>

namespace compress {
    void encode(std::istream &in, std::ostream &out);

    void decode(std::istream &in, std::ostream &out);
}

#endif //COMPRESSION_UTILTITY_COMPRESSION_H
