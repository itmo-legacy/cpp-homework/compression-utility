#include "../include/compression.h"

#include <cstddef>  // size_t
#include <memory>   // shared_ptr
#include <cassert>
#include <queue>
#include <climits>  // CHAR_BIT
#include <cstring>  // memcpy
#include <vector>
#include <array>
#include <algorithm>
#include <map>

using std::size_t;
using std::vector;

using char_t = unsigned char;
using frequency_t = unsigned;
using length_t = char_t;

static_assert(std::is_unsigned_v<length_t>);

static constexpr size_t ALPHABET_SIZE = 1U << std::numeric_limits<char_t>::digits;

struct buffered_istream {
    static constexpr size_t BUFFER_SIZE = 1U << 18U;
    char buffer[BUFFER_SIZE];
    size_t pos;
    std::streamsize size;
    std::istream &stream;

    buffered_istream(std::istream &stream) : stream(stream) {
        fill_buffer();
    }

    void fill_buffer() {
        stream.read(buffer, BUFFER_SIZE);
        size = stream.gcount();
        pos = 0;
    }

    template<typename T>
    bool read_binary(T &portion) {
        constexpr size_t TSIZE = sizeof(T);
        if (size - pos >= TSIZE) {
            std::memcpy(&portion, buffer + pos, TSIZE);
            pos += TSIZE;
        } else {
            size_t shift = size - pos;
            std::memcpy(&portion, buffer + pos, shift);
            fill_buffer();
            if (size < TSIZE - shift) { return false; }
            std::memcpy(&portion + shift, buffer + pos, TSIZE - shift);
            pos += TSIZE - shift;
        }
        return true;
    }

    void rewind() {
        stream.clear();
        stream.seekg(0);
    }
};

struct buffered_ostream {
    static constexpr size_t BUFFER_SIZE = 1U << 16U;
    char buffer[BUFFER_SIZE];
    size_t pos = 0;
    std::ostream &stream;

    buffered_ostream(std::ostream &stream) : stream(stream) {
    }

    template<typename T>
    void
    write_binary(T const &portion, unsigned n = sizeof(T)) { // assume little endian
        char chars[sizeof(T)];
        std::memcpy(chars, &portion, sizeof(T));
        std::reverse(chars, chars + sizeof(T));
        if (n <= BUFFER_SIZE - pos) {
            std::memcpy(buffer + pos, chars, n);
            pos += n;
        } else {
            size_t shift = BUFFER_SIZE - pos;
            std::memcpy(buffer + pos, chars, shift);
            pos += shift;
            empty_buffer();
            std::memcpy(buffer + pos, chars + shift, n - shift);
            pos += n - shift;
        }
    }

    void empty_buffer() {
        stream.write(buffer, pos);
        pos = 0;
    }

    ~buffered_ostream() {
        empty_buffer();
    }
};

struct code_t {
    using pack_t = std::uint64_t;
    length_t length = 0;
    vector<pack_t> packs = {0}; // little-endian, should never be empty

    void increment() {
        int carry = 1;
        for (size_t i = 0; i < packs.size() and carry; ++i) {
            carry = ++packs[i] == 0;
        }
        if (carry) { packs.emplace_back(1); }
    }

    void shift() {
        pack_t carry = 0;
        for (auto &bit : packs) {
            auto new_carry = bit >> (std::numeric_limits<pack_t>::digits - 1U);
            bit = (bit << 1U) | carry;
            carry = new_carry;
        }
        if (carry) { packs.emplace_back(1); }
    }

    void add(unsigned n) {
        pack_t old_value = packs[0];
        packs[0] += n;
        if (packs[0] < old_value) { increment(); }
    }

    void reverse_bits() {
        auto last_bit_length = length % std::numeric_limits<pack_t>::digits;
        auto shift = std::numeric_limits<pack_t>::digits - last_bit_length;
        for (size_t i = packs.size(); i-- > 1;) {
            packs[i] = packs[i] << shift | packs[i - 1] >> last_bit_length;
        }
        packs[0] <<= shift;
        std::reverse(packs.begin(), packs.end());
    }

    bool operator<(code_t const &rhs) const {
        if (length != rhs.length) { return length < rhs.length; }
        else { return packs < rhs.packs; }
    }
};

struct node_t {
    char_t data;
    frequency_t frequency;
    std::shared_ptr<node_t> lchild = nullptr; // can't use unique_ptr because priority_queue requires copy assignment
    std::shared_ptr<node_t> rchild = nullptr;

    node_t() = default;

    node_t(char_t data, frequency_t frequency) noexcept : data(data), frequency(frequency) {}

    node_t(node_t smaller, node_t bigger)
            : data(0), frequency(smaller.frequency + bigger.frequency)
              , lchild(std::make_shared<node_t>(smaller)), rchild(std::make_shared<node_t>(bigger)) {}

    bool is_leaf() const { return not lchild and not rchild; }
};

struct reader {
    buffered_istream &in;

    char_t bytes[3];
    length_t pos;

    bool no_bytes = false;
    bool eof = false;

    explicit reader(buffered_istream &in) : in(in) {
        slurp_byte();
        slurp_byte();
        slurp_byte();  // slurp three bytes to fill the array
    }

    bool has_next_bit() {
        return not(no_bytes and eof);
    }

    char_t next_bit() {
        assert(has_next_bit());
        char_t result = (bytes[0] >> (CHAR_BIT - 1 - pos)) & 1;
        proceed();
        return result;
    }

    void slurp_byte() {
        bytes[0] = bytes[1];
        bytes[1] = bytes[2];
        no_bytes = not in.read_binary(bytes[2]);
        pos = 0;
    }

    void proceed() {
        if (no_bytes and pos >= bytes[1] - 1) {
            eof = true;
        } else if (pos == CHAR_BIT - 1) {
            slurp_byte();
        } else {
            ++pos;
        }
    }
};

void
fill(vector<length_t> &table, length_t length, node_t const &tree) {
    if (tree.is_leaf()) {
        table[tree.data] = length;
        return;
    }
    fill(table, length_t(length + 1), *tree.lchild);
    fill(table, length_t(length + 1), *tree.rchild);
}

vector<frequency_t>
get_frequency_table(buffered_istream &in) {
    vector<frequency_t> table(ALPHABET_SIZE);
    char_t portion;
    while (in.read_binary(portion)) {
        ++table[portion];
    }
    in.rewind();
    return table;
}

node_t
get_huffman_tree(std::array<node_t, ALPHABET_SIZE> const &nodes) {
    auto comp = [](node_t const &one, node_t const &two) { return one.frequency > two.frequency; };
    std::priority_queue<node_t, vector<node_t>, decltype(comp)> queue(nodes.begin(), nodes.end(), comp);
    while (queue.size() > 1) {
        auto smaller = queue.top();
        queue.pop();
        auto bigger = queue.top();
        queue.pop();
        node_t merged = node_t(smaller, bigger);
        queue.push(merged);
    };
    assert(queue.size() == 1);
    return queue.top();
}

vector<length_t>
get_code_lengths(node_t const &tree) {
    vector<length_t> result(ALPHABET_SIZE, 0);
    fill(result, 0, tree);
    return result;
}

vector<code_t>
get_canonical_encoding(vector<length_t> const &lengths) {
    assert(lengths.size() == ALPHABET_SIZE);
    vector<code_t> result(ALPHABET_SIZE);
    vector<char_t> portions(ALPHABET_SIZE);
    for (size_t i = 0; i < lengths.size(); ++i) { portions[i] = char_t(i); }
    std::sort(portions.begin(), portions.end(),
              [&lengths](auto const &one, auto const &two) { return lengths[one] < lengths[two]; });
    length_t code_length = lengths[portions[0]];
    code_t current;
    current.length = code_length;
    for (auto &&portion: portions) {
        while (lengths[portion] > code_length) {
            current.shift();
            ++code_length;
        }
        current.length = code_length;
        result[portion] = current;
        current.increment();
    }
    return result;
}

void
print_encoded_symbols(buffered_ostream &out, code_t::pack_t &buffer, unsigned &bit_count, code_t &code) {
    static constexpr auto digits = std::numeric_limits<code_t::pack_t>::digits;
    unsigned shift = digits - bit_count;
    auto &packs = code.packs;

    for (size_t i = 0; i < packs.size() - 1; ++i) {
        out.write_binary(buffer | (packs[i] >> bit_count));
        buffer = packs[i] << shift;
    }
    buffer |= packs.back() >> bit_count;
    bit_count += code.length;
    if (bit_count >= digits) {
        out.write_binary(buffer);
        buffer = packs.back() << shift;
        bit_count -= digits;
    }
}

void
print_encoded(vector<code_t> &encoding_table, buffered_istream &in, buffered_ostream &out) {
    char_t portion;
    unsigned bit_count = 0;
    code_t::pack_t buffer = 0;
    bool empty = true;
    while (in.read_binary(portion)) {
        empty = false;
        code_t &code = encoding_table[portion];
        print_encoded_symbols(out, buffer, bit_count, code);
    }
    if (bit_count != 0) {
        out.write_binary(buffer, (bit_count - 1) / CHAR_BIT + 1);
    }
    char_t count = char_t(bit_count % CHAR_BIT);     // so as to decode how many bits in last byte are not used
    if (count == 0 and not empty) { count = CHAR_BIT; }
    out.write_binary(count);
}

void
compress::encode(std::istream &in, std::ostream &out) {
    buffered_istream bin(in);
    buffered_ostream bout(out);
    auto frequency_table = get_frequency_table(bin);
    std::array<node_t, ALPHABET_SIZE> nodes;
    for (size_t i = 0; i < nodes.size(); ++i) { nodes[i] = {char_t(i), frequency_table[i]}; }

    auto huffman_tree = get_huffman_tree(nodes);
    auto code_lengths = get_code_lengths(huffman_tree);
    auto code_table = get_canonical_encoding(code_lengths);

    for (auto &&length: code_lengths) { bout.write_binary(length); }
    for (auto &code: code_table) { code.reverse_bits(); }
    print_encoded(code_table, bin, bout);
}

vector<length_t>
get_code_lengths(buffered_istream &in) {
    vector<length_t> lengths(ALPHABET_SIZE);
    for (size_t i = 0; i < ALPHABET_SIZE; ++i) { in.read_binary(lengths[i]); }
    return lengths;
}

bool decode_symbol(char_t &symbol, vector<unsigned int> const &lengths,
                   std::map<code_t, char_t> const &char_table, reader &reader) {
    code_t code;          // code being decoded
    code_t first;         // first code of length bit_count

    for (length_t bit_count = 1; bit_count != 0; ++bit_count) {
        if (not reader.has_next_bit()) { return false; }
        code.length = first.length = bit_count;
        unsigned codes_count = lengths[bit_count]; // number of codes of length bit_count
        char_t bit = reader.next_bit();
        if (bit) { code.increment(); }
        first.add(codes_count);
        if (code < first) {
            symbol = char_table.at(code);
            return true;
        }
        first.shift();
        code.shift();
    }
    return false;
}

void
print_decoded(vector<unsigned int> const &lengths, std::map<code_t, char_t> const &char_table,
              buffered_istream &in, std::ostream &out) {
    char_t symbol;
    reader reader(in);
    while (decode_symbol(symbol, lengths, char_table, reader)) {
        out << symbol;
    }
}

vector<unsigned>
group_by_count(vector<length_t> const &lengths) {
    vector<unsigned> result(ALPHABET_SIZE);
    for (auto &&length: lengths) { ++result[length]; }
    return result;
}

void
compress::decode(std::istream &in, std::ostream &out) {
    buffered_istream bin(in);
    auto code_lengths = get_code_lengths(bin);
    auto code_table = get_canonical_encoding(code_lengths);

    auto lengths = group_by_count(code_lengths);
    std::map<code_t, char_t> char_table;     // TODO: replace with unordered_map
    for (size_t ch = 0; ch < code_table.size(); ++ch) { char_table[code_table[ch]] = char_t(ch); }
    print_decoded(lengths, char_table, bin, out);
}
